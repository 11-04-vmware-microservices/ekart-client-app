package com.classpath.ekartoauth2clientapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/userinfo")
@RequiredArgsConstructor
public class UserInfoController {
    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
    @GetMapping
    public Map<String, String> fetchUserInfo(OAuth2AuthenticationToken auth2AuthenticationToken){
        Map<String, String> responseMap = new LinkedHashMap<>();
        OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService
                .loadAuthorizedClient(auth2AuthenticationToken.getAuthorizedClientRegistrationId(), auth2AuthenticationToken.getPrincipal().getName());
        OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();

        String tokenValue = accessToken.getTokenValue();
        Set<String> scopes = accessToken.getScopes();
        Instant issuedAt = accessToken.getIssuedAt();
        Instant expiresAt = accessToken.getExpiresAt();

        responseMap.put("access-token", tokenValue);
        responseMap.put("scopes", scopes.toString());
        responseMap.put("issued-at", issuedAt.atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME));
        responseMap.put("expires-at", expiresAt.atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME));
        return responseMap;
    }
}
