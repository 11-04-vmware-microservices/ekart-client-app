package com.classpath.ekartoauth2clientapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/login**", "/logout**", "/oauth2/**", "/oauth2**")
                    .permitAll()
                .anyRequest()
                    .authenticated()
                .and()
                    .oauth2Login();

    }
}
